const online = document.querySelector('#online');
const offline = document.querySelector('#offline');
const msg = document.querySelector('#msg');
const send = document.querySelector('#send');

const socket = io();

socket.on('connect', () => {
  console.log('Conectado');
  offline.style.display = 'none';
  online.style.display = '';
});

socket.on('disconnect', () => {
  console.log('Desconectado');
  offline.style.display = '';
  online.style.display = 'none';
});

socket.on('send_message', (payload) => {
  console.log(payload);
});

send.addEventListener('click', () => {
  const message = msg.value;
  const payload = {
    message,
    id: 12,
    date: new Date().getTime()
  }
  socket.emit('send_message', payload, (id) => {
    console.log('Desde el servidor: ' + id);
  });
})