const socketController = (socket) => {
  console.log('Cliente conectado', socket.id);

  socket.on('disconnect', () => {
    console.log('Cliente desconectado', socket.id);
  });

  socket.on('send_message' , (payload, callback) => {
    console.log(payload);
    const id = 123456;
    callback(id);
    // socket.broadcast.emit('send_message', payload);
  });
}

module.exports = {
  socketController
}